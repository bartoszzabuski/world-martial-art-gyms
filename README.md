##World Marital Arts Gyms

Small project to demonstrate webservice used to find martial arts gyms
across the world. 

###Technologies used: 
- Java8
- Graphql
- spring-boot

###Building: 
mvn clean install

###Running:
- in dev mode -> mvn spring-boot:run -Dspring-boot.run.profiles=dev (populates the data on startup. see com.wmag.gymservice.config.OnInitDataLoader)

###Pre-requisites: 
- MongoDB running (see application.yml to configure connection details)***

###Things to properly explore:
- more advanced queries
- validation
- error handling

*** run:
1. docker pull mongo then 
2. docker run --name gyms-db --restart=always -d -p 27017:27017 mongo mongod

