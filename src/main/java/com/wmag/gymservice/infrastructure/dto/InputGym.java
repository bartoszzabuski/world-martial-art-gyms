package com.wmag.gymservice.infrastructure.dto;

import com.wmag.gymservice.domain.Activity;
import lombok.*;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InputGym {

    private String name;

    private String shortDescription;

    private InputPoint position;

    private Set<Activity> activities;

    @Value
    @Getter
    public static class InputPoint {
        double x;
        double y;
    }
}
