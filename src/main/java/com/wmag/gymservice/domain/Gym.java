package com.wmag.gymservice.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Set;

import static com.wmag.gymservice.domain.Gym.GYMS_COLLECTION;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document(collection = GYMS_COLLECTION)
public class Gym {

    public static final String GYMS_COLLECTION = "gyms";

    @Id
    private ObjectId id;

    private String name;

    private String shortDescription;

    @GeoSpatialIndexed(name = "position")
    private Point position;

    private Set<Activity> activities;

}
