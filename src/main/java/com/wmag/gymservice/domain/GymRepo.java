package com.wmag.gymservice.domain;


import org.bson.types.ObjectId;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface GymRepo extends PagingAndSortingRepository<Gym, ObjectId> {

    Gym findByName(String firstName);

}
