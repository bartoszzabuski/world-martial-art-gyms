package com.wmag.gymservice.config;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.wmag.gymservice.domain.Gym;
import com.wmag.gymservice.domain.GymRepo;
import com.wmag.gymservice.infrastructure.exceptions.GymNotFoundException;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class Query implements GraphQLQueryResolver {
    private final GymRepo gymRepo;

    @Autowired
    public Query(GymRepo gymRepo) {
        this.gymRepo = gymRepo;
    }

    public List<Gym> gyms() {
        return (List<Gym>) gymRepo.findAll();
    }

    public Gym gym(String id) {
        return gymRepo.findById(new ObjectId(id)).orElseThrow(GymNotFoundException::new);
    }

//    public Gym getGymByName(String name) {
//        return gymRepo.findByName(name);
//    }
}