package com.wmag.gymservice.config;

import com.wmag.gymservice.domain.GymRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

// TODO used only for initial testing during development. Delete it eventually!
@Profile("dev")
@Component
public class OnInitDataLoader extends DataLoader{

    @Autowired
    public OnInitDataLoader(GymRepo gymRepo) {
        super(gymRepo);
    }

    @PostConstruct
    public void init(){
        super.generateFixedData();
    }
}
