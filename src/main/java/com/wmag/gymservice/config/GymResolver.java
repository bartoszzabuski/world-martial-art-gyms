package com.wmag.gymservice.config;


import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.wmag.gymservice.domain.Gym;
import com.wmag.gymservice.domain.GymRepo;
import com.wmag.gymservice.infrastructure.dto.InputGym;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Point;
import org.springframework.stereotype.Component;

@Component
public class GymResolver implements GraphQLMutationResolver {

    private final GymRepo gymRepo;

    @Autowired
    public GymResolver(GymRepo gymRepo) {
        this.gymRepo = gymRepo;
    }

    public Gym createGym(InputGym inputGym) {
        // TODO add some sort of validation
        return gymRepo.save(Gym.builder()
                .name(inputGym.getName())
                .position(new Point(inputGym.getPosition().getX(), inputGym.getPosition().getY()))
                .shortDescription(inputGym.getShortDescription())
                .activities(inputGym.getActivities())
                .build());
    }
}
