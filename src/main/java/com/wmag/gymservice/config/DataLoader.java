package com.wmag.gymservice.config;

import com.wmag.gymservice.domain.Gym;
import com.wmag.gymservice.domain.GymRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Point;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static com.wmag.gymservice.domain.Activity.*;
import static java.util.Arrays.asList;


// TODO used only for initial testing during development. Delete it eventually or move to test package!
public class DataLoader {

    public static final Gym ALICES_GYM = Gym.builder()
            .name("AlicesGym")
            .shortDescription("best gym eva:P")
            .activities(new HashSet(asList(BOXING, BJJ, KICKBOXING)))
            .position(new Point(51.517708, -0.137061))
            .build();
    public static final Gym TOMS_GYM = Gym.builder()
            .name("TomsGym")
            .shortDescription("second best gym eva:P")
            .activities(new HashSet(asList(BOXING, THAI_BOXING, MMA)))
            .position(new Point(51.517373, -0.103648))
            .build();
    public static final Gym RICHARDS_GYM = Gym.builder()
            .name("RichardsGym")
            .shortDescription("third best gym eva:P")
            .activities(new HashSet(asList(BOXING, BJJ, KICKBOXING)))
            .position(new Point(51.499938, -0.099583))
            .build();
    private final GymRepo gymRepo;


    @Autowired
    public DataLoader(GymRepo gymRepo) {
        this.gymRepo = gymRepo;
    }

    public void generateFixedData() {

        gymRepo.deleteAll();

        List<Gym> gyms = new ArrayList();
        gyms.add(ALICES_GYM);
        gyms.add(TOMS_GYM);
        gyms.add(RICHARDS_GYM);

        gymRepo.saveAll(gyms);

        // fetch all Gyms
        System.out.println("Gyms found with findAll():");
        System.out.println("-------------------------------");
        for (Gym Gym : gymRepo.findAll()) {
            System.out.println(Gym);
        }
        System.out.println();

        // fetch an individual Gym
        System.out.println("Gym found with Name('AlicesGym'):");
        System.out.println("--------------------------------");
        System.out.println(gymRepo.findByName("AlicesGym"));
    }

    public void clearDB() {
        gymRepo.deleteAll();
    }
}
