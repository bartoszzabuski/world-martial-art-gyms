package com.wmag.gymservice.e2e.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.assertj.core.util.Strings;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.StreamUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

import static java.lang.String.format;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.MediaType.APPLICATION_JSON;

/*
 class heavily based on com.okta.springbootgraphql.GraphqlTestUtils from https://github.com/okta/okta-spring-boot
 as I couldn't make util classes from graphql-spring-boot-starter-test work. However, keep an eye on graphql-spring-boot-starter-test
 library as it should mature soon
*/
@Component
public class GraphqlTestUtils {

    private static HttpHeaders HEADERS = new HttpHeaders();

    static {
        HEADERS.setContentType(APPLICATION_JSON);
    }

    @Value("${graphql.servlet.mapping:/graphql}")
    private String url;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private ResourceLoader resourceLoader;


    public TestGraphQLResponse run(String payloadPath, String... args) throws IOException {
        HttpEntity<String> httpEntity = new HttpEntity<>(payload(payloadPath, args), HEADERS);
        ResponseEntity<String> response = restTemplate.exchange(url, POST, httpEntity, String.class);
        return new TestGraphQLResponse(response.getStatusCode(), parse(response.getBody()));
    }

    @NotNull
    private String payload(String path, String... args) throws IOException {
        Resource resource = resourceLoader.getResource("classpath:" + path);
        return loadResource(resource, args);
    }

    private String loadResource(Resource resource, String... args) throws IOException {
        try (InputStream inputStream = resource.getInputStream()) {
            String template = StreamUtils.copyToString(inputStream, UTF_8);
            return format(template, args);
        }
    }

    private Optional<JsonNode> parse(String payload) throws IOException {
        if (Strings.isNullOrEmpty(payload)) {
            return Optional.empty();
        }
        return Optional.of(new ObjectMapper().readTree(payload));
    }

    @Getter
    @AllArgsConstructor
    public static class TestGraphQLResponse {
        private HttpStatus statusCode;
        private Optional<JsonNode> payload;
    }

}
