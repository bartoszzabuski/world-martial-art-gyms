package com.wmag.gymservice.e2e.utils;

import com.wmag.gymservice.config.DataLoader;
import com.wmag.gymservice.domain.GymRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TestDataLoader extends DataLoader {

    @Autowired
    TestDataLoader(GymRepo gymRepo) {
        super(gymRepo);
    }
}
