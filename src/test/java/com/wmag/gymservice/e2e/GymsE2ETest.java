package com.wmag.gymservice.e2e;

import com.fasterxml.jackson.databind.JsonNode;
import com.wmag.gymservice.domain.Activity;
import com.wmag.gymservice.e2e.utils.GraphqlTestUtils;
import com.wmag.gymservice.e2e.utils.TestDataLoader;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import static com.wmag.gymservice.e2e.utils.GraphqlTestUtils.TestGraphQLResponse;
import static com.wmag.gymservice.e2e.utils.TestDataLoader.ALICES_GYM;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class GymsE2ETest {

    private static final String NEW_GYM_NAME = "newGymName";
    private static final int DELTA = 0;
    @Autowired
    private GraphqlTestUtils graphqlTestUtils;
    @Autowired
    private TestDataLoader dataLoader;

    @Before
    public void setup() {
        dataLoader.generateFixedData();
    }

    @After
    public void cleanup() {
        dataLoader.clearDB();
    }

    @Test
    public void testAllGymsCanBeFetched() throws IOException {

        TestGraphQLResponse response = graphqlTestUtils.run("e2e/get-gyms.graphql");

        assertNotNull(response);
        assertEquals(response.getStatusCode(), HttpStatus.OK);
        JsonNode payload = response.getPayload().get();
        assertNotNull(payload.get("data"));
        assertEquals(payload.get("data").get("gyms").get(0).get("name").textValue(), "AlicesGym");
        assertNotNull(payload.get("data").get("gyms").get(0).get("id").textValue());
        assertEquals(payload.get("data").get("gyms").get(1).get("name").textValue(), "TomsGym");
        assertNotNull(payload.get("data").get("gyms").get(1).get("id").textValue());
        assertEquals(payload.get("data").get("gyms").get(2).get("name").textValue(), "RichardsGym");
        assertNotNull(payload.get("data").get("gyms").get(2).get("id").textValue());

    }

    @Test
    public void testCreateGym() throws IOException {

        TestGraphQLResponse response = graphqlTestUtils.run("e2e/create-gym.graphql",
                NEW_GYM_NAME);

        assertNotNull(response);
        assertEquals(response.getStatusCode(), HttpStatus.OK);
        JsonNode payload = response.getPayload().get();
        assertNotNull(payload.get("data"));
        assertEquals(payload.get("data").get("createGym").get("name").textValue(), NEW_GYM_NAME);
        assertNotNull(payload.get("data").get("createGym").get("id"));

    }

    @Test
    public void testQueryGym() throws IOException {

        TestGraphQLResponse response = graphqlTestUtils.run("e2e/query-gym.graphql", ALICES_GYM.getId().toString());

        assertNotNull(response);
        assertEquals(response.getStatusCode(), HttpStatus.OK);
        JsonNode payload = response.getPayload().get();
        assertNotNull(payload.get("data"));
        assertEquals(ALICES_GYM.getName(), payload.get("data").get("gym").get("name").textValue());
        assertEquals(ALICES_GYM.getId().toString(), payload.get("data").get("gym").get("id").textValue());
        assertEquals(ALICES_GYM.getShortDescription(), payload.get("data").get("gym").get("shortDescription").textValue());
        assertEquals(ALICES_GYM.getPosition().getX(), payload.get("data").get("gym").get("position").get("x").doubleValue(), DELTA);
        assertEquals(ALICES_GYM.getPosition().getY(), payload.get("data").get("gym").get("position").get("y").doubleValue(), DELTA);
        assertEquals(ALICES_GYM.getActivities(), toActivities(payload.get("data").get("gym").get("activities")));

    }

    private Set<Activity> toActivities(JsonNode jsonNode) {
        if (!jsonNode.isArray()) {
            return null;
        }
        Set<Activity> activityList = new HashSet<>(jsonNode.size());
        for (final JsonNode objNode : jsonNode) {
            activityList.add(Activity.parse(objNode.textValue()));
        }
        return activityList;
    }
}
